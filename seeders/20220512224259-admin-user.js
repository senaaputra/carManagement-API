"use strict";
const { encryptPassword } = require("../controller/api/v1/auth");

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert("Users", [
      {
        fullname: "admin",
        email: "admin@gmail.com",
        encryptedPassword: await encryptPassword("123456"),
        phone: "09875743535",
        userRoleId: 2,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        fullname: "member",
        email: "member@gmail.com",
        encryptedPassword: await encryptPassword("123456"),
        phone: "99080775123",
        userRoleId: 3,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  async down(queryInterface, Sequelize) {
    return queryInterface.bulkDelete("Users", null, {});
  },
};
