"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    return queryInterface.bulkInsert("UserRoles", [
      {
        role_name: "Superadmin",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        role_name: "Admin",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        role_name: "Member",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  async down(queryInterface, Sequelize) {
    return queryInterface.bulkDelete("UserRoles", null, {});
  },
};
