"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    return queryInterface.bulkInsert("Cars", [
      {
        car_name: "Tesla Model Y",
        car_type: "electric car",
        car_price: 700000,
        car_size: "Small",
        image: "sportagejpg.jpg",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        car_name: "Hyundai Ioniq 5",
        car_type: "Hyundai",
        car_price: 500000,
        car_size: "Small",
        image: "60f592b49421e.jpg",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        car_name: "Toyota Fortuner",
        car_type: "Toyota",
        car_price: 400000,
        car_size: "Large",
        image: "fortuner.jpg",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  async down(queryInterface, Sequelize) {
    return queryInterface.bulkDelete("Cars", null, {});
  },
};
