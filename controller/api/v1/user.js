const { user } = require("mysql2");
const userRepository = require("../../../repositories/userRepository");
const userService = require("../../../services/userService");

module.exports = {
  async register(req, res) {
    const result = await userService.create(req);
    res.status(200).json({
      status: "OK",
      data: result,
      message: "User successfully created",
    });
  },

  async registerAdmin(req, res) {
    try {
      const result = await userService.createAdmin(req);
      res.json({
        data: result,
        message: "Admin successfully created",
      });
    } catch (err) {
      console.log(err.message);
      res.json({
        message: "UNAUTHORIZED ACCESS TO CREATE AN ADMIN",
      });
    }
  },

  async registerSA(req, res) {
    const result = await userService.createSA(req);
    res.status(200).json({
      status: "OK",
      data: result,
      message: "SuperAdmin successfully created",
    });
  },

  async login(req, res, next) {
    try {
      const result = await userService.check(req, res);

      const { data } = result;

      if (!data) {
        res.status(401).json({
          status: "Failed",
          success: false,
          err: true,
          data: null,
          message: "User not found",
        });
        return;
      }
      // const info = {
      //   id: data.id,
      //   email: data.email,
      //   name: data,
      //   fullname,
      //   userRole: data.userRoles.role_name,
      //   token,
      // };

      res.status(201).json({
        status: "OK",
        success: true,
        data: data,
        message: "Login Success...",
      });
    } catch (err) {
      console.log(err.message);
      // res.status(401).json({
      //   status: "Failed",
      //   success: false,
      //   err: true,
      //   data: null,
      //   message: "Unauthorized",
      // });
    }
  },

  async authorize(req, res, next) {
    try {
      const hasil = await userService.authorizeUser(req);
      if (!hasil.data) {
        res.json({
          message: "UNAUTHORIZED",
        });
      }
      req.nameAuthorize = hasil.data.fullname;
      req.roleAuthorize = hasil.data.userRoleId;
      req.roleId = hasil.data.id;
      req.hasil = hasil;
      next();
    } catch {
      console.log("ERROR GETTING CAR LIST");
      res.status(403).json({
        message: "ERROR AUTHORIZING",
      });
    }
  },

  async authorizeUsers(req, res) {
    try {
      const hasil = await req.hasil;
      res.status(200).json({
        AccessedBy: hasil.data.fullname,
        Role: hasil.user[0].userRoles.role_name,
      });
    } catch {
      res.status(404).json({
        message: "error on geting user details",
      });
    }
  },

  async authorizeAdmin(req, res, next) {
    try {
      const hasil = await userService.authorizeAdmin(req);
      if (!hasil.data) {
        res.json({
          message: "UNAUTHORIZED",
        });
      }
      req.nameAuthorizeAdmin = hasil.data.fullname;
      req.roleAuthorizeAdmin = hasil.data.userRoleId;
      req.roleId = hasil.data.id;
      req.hasil = hasil;
      next();
    } catch {
      res.status(500).json({
        message: "Internal Server Error",
      });
    }
  },

  async authorizeSuperAdmin(req, res, next) {
    try {
      const hasil = await userService.authorizeSA(req);
      if (!hasil.data) {
        res.json({
          message: "UNAUTHORIZED ACCESS TO CREATE AN ADMIN",
        });
      }
      req.nameAuthorizeSA = hasil.data.fullname;
      req.roleAuthorizeSA = hasil.data.userRoleId;
      req.roleId = hasil.data.id;
      next();
    } catch {
      res.status(403).json({
        message: "ERROR AUTHORIZING",
      });
    }
  },

  async deleteUser(req, res) {
    try {
      const result = await userService.deleteUser(req.params.id);
      res.json({
        message: "User Berhasil di delete oleh admin",
      });
    } catch (error) {
      console.log(error);
      res.status(403).json({
        message: "ERROR AUTHORIZING",
      });
    }
  },

  async deleteAdmin(req, res) {
    try {
      const result = await userService.deleteAdmin(req.params.id);
      res.json({
        message: "User Berhasil di delete oleh admin",
      });
    } catch (error) {
      console.log(error);
      res.status(403).json({
        message: "ERROR AUTHORIZING",
      });
    }
  },
};
