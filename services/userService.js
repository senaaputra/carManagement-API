const userRepository = require("../repositories/userRepository");
const auth = require("../controller/api/v1/auth");
const jwt = require("jsonwebtoken");
const userrole = require("../models");

module.exports = {
  async create(requestBody) {
    const fullname = requestBody.body.fullname;
    const email = requestBody.body.email;
    const pass = requestBody.body.password;
    const phone = requestBody.body.phone;

    const encryptedPassword = await auth.encryptPassword(pass);

    return userRepository.create({
      name: fullname,
      email,
      encryptedPassword,
      phone,
      userRoleId: 3,
    });
  },

  async createAdmin(requestBody) {
    const fullname = requestBody.body.fullname;
    const email = requestBody.body.email;
    const pass = requestBody.body.password;
    const phone = requestBody.body.phone;

    const encryptedPassword = await auth.encryptPassword(pass);

    return userRepository.create({
      name: fullname,
      email,
      encryptedPassword,
      phone,
      userRoleId: 2,
    });
  },

  async createSA(requestBody) {
    const fullname = requestBody.body.fullname;
    const email = requestBody.body.email;
    const pass = requestBody.body.password;
    const phone = requestBody.body.phone;

    const encryptedPassword = await auth.encryptPassword(pass);

    return userRepository.create({
      name: fullname,
      email,
      encryptedPassword,
      phone,
      userRoleId: 1,
    });
  },

  async check(req, res) {
    try {
      const email = req.body.email.toLowerCase();
      const pass = req.body.password;

      const user = await userRepository.findOne({
        where: { email },
        include: ["userRoles"],
      });

      if (!user) {
        return { message: "Can't find that account" };
      }

      const isPasswordCorrect = await auth.comparePassword(
        user[0].encryptedPassword,
        pass
      );

      if (!isPasswordCorrect) {
        return { message: "Can't find that account" };
      }

      const token = auth.createToken({
        id: user[0].id,
        email: user[0].email,
        name: user[0].fullname,
        phone: user[0].phone,
        role_name: user[0].userRoleId,
      });

      const newToken = res.json({
        id: user[0].id,
        name: user[0].fullname,
        phone: user[0].phone,
        Identification: user[0].userRoles.role_name,
        token,
      });

      return newToken;
    } catch (error) {
      console.log(error);
    }
  },

  async whoAmI(req, res) {
    res.status(200).json(requestUser);
  },

  async authorize(req, res, next) {
    try {
      const bearerToken = req.headers.authorization;
      const token = bearerToken.split("Bearer ")[1];
      const tokenPayload = jwt.verify(
        token,
        process.env.JWT_SIGNATURE_KEY || "Rahasia"
      );

      requestUser = await userRepository.findID(tokenPayload.id);
      user = await userRepository.findOne({
        where: { id },
        include: ["userRoles"],
      });
      console.log(requestUser.dataValues.userRoleId);
      if (requestUser.userRoleId) {
        return {
          data: requestUser,
          user: user,
        };
      }
    } catch (err) {
      return {
        data: null,
      };
    }
  },

  async authorizeAdmin(req, res, next) {
    try {
      const bearerToken = req.headers.authorization;
      const token = bearerToken.split("Bearer ")[1];
      const tokenPayload = jwt.verify(
        token,
        process.env.JWT_SIGNATURE_KEY || "Rahasia"
      );
      const id = tokenPayload.id;
      requestUser = await userRepository.findID(tokenPayload.id, {
        include: "userRoles",
      });
      user = await userRepository.findOne({
        where: { id },
        include: ["userRoles"],
      });
      if (requestUser.userRoleId == 1 || requestUser.userRoleId == 2) {
        return {
          data: requestUser,
          user: user,
        };
      }
    } catch (err) {
      return {
        data: null,
      };
    }
  },

  async authorizeUser(req, res, next) {
    try {
      const bearerToken = req.headers.authorization;
      const token = bearerToken.split("Bearer ")[1];
      const tokenPayload = jwt.verify(
        token,
        process.env.JWT_SIGNATURE_KEY || "Rahasia"
      );
      const id = tokenPayload.id;
      requestUser = await userRepository.findID(tokenPayload.id, {
        include: "userRoles",
      });
      user = await userRepository.findOne({
        where: { id },
        include: ["userRoles"],
      });
      if (requestUser.userRoleId) {
        return {
          data: requestUser,
          user: user,
        };
      }
    } catch (err) {
      return {
        data: null,
      };
    }
  },

  async authorizeSA(req, res, next) {
    try {
      const bearerToken = req.headers.authorization;
      const token = bearerToken.split("Bearer ")[1];
      const tokenPayload = jwt.verify(
        token,
        process.env.JWT_SIGNATURE_KEY || "Rahasia"
      );

      requestUser = await userRepository.findID(tokenPayload.id);
      console.log("REQ USER", requestUser.userRoleId);
      if (requestUser.userRoleId == 1) {
        return {
          data: requestUser,
        };
      }
    } catch (err) {
      return {
        data: null,
      };
    }
  },

  async deleteUser(id) {
    return userRepository.deleteUser(id);
  },

  async deleteAdmin(id) {
    return userRepository.deleteAdmin(id);
  },
};
